import axios from 'axios'
import path from 'path'
import FormData from 'form-data'
import { server } from '../config'
import AsyncStorage from '@react-native-community/async-storage'

const likeProcess = () => {
    return {
        type: 'LIKE_PROCESS'
    }
}

const likeSuccess = () => {
    return {
        type: 'LIKE_SUCCESS'
    }
}

const commentProcess = () => {
    return {
        type: 'COMMENT_PROCESS'
    }
}

const commentSuccess = () => {
    return {
        type: 'COMMENT_SUCCESS'
    }
}


const feedProcess = () => {
    return {
        type: 'FEED_PROCESS'
    }
}

const feedSuccess = (data) => {
    return {
        type: 'FEED_SUCCESS',
        payload: data
    }
}

const feedFailed = (data) => {
    return {
        type: 'FEED_FAILED',
        payload: data
    }
}

const upFeedProcess = () => {
    return {
        type: 'UPFEED_PROCESS'
    }
}

const upFeedSuccess = (data) => {
    return {
        type: 'UPFEED_SUCCESS',
        payload: data
    }
}

const upFeedFailed = (data) => {
    return {
        type: 'UPFEED_FAILED',
        payload: data
    }
}

export const getFeed = (refresh) => {
    const url = `${server}/feed`
    return dispatch => {
        if (refresh || refresh == undefined) {
            dispatch(feedProcess())
        }
        AsyncStorage
            .getItem('USER')
            .then(result => {
                return JSON.parse(result)
            })
            .then(result => {
                axios
                    .post(url, {
                        email: result.email,
                        password: result.password
                    })
                    .then(response => dispatch(feedSuccess(response.data)))
                    .catch((err) => {
                        if (!err.response) {
                            dispatch(feedFailed('Connection Error'))
                        } else {
                            dispatch(feedFailed(err.response.data.message))
                        }
                    })
            })
    }
}

export const imageSet = (source) => {
    return {
        type: 'IMAGE_TAKE',
        payload: source
    }
}

export const imageRemove = () => {
    return {
        type: 'IMAGE_DELETE'
    }
}

export const newFeed = (caption, image) => {
    let url = `${server}/feed`
    const { uri } = image
    let data = new FormData()
    data.append('picture',
        {
            uri: uri,
            name: path.basename(uri),
            type: `image/${(path.extname(uri)).slice(1)}`
        }
    )
    return dispatch => {
        dispatch(upFeedProcess())
        AsyncStorage
            .getItem('USER')
            .then(result => {
                return JSON.parse(result)
            })
            .then(result => {
                axios
                    .put(url, data, {
                        headers: {
                            email: result.email,
                            password: result.password,
                            caption
                        }
                    })
                    .then(() => dispatch(getFeed()))
                    .catch(err => console.log(err.response.data))
            })
    }
}

export const likeFeed = (feedId) => {
    const url = `${server}/like`
    return dispatch => {
        dispatch(likeProcess())
        AsyncStorage
            .getItem('USER')
            .then(result => {
                return JSON.parse(result)
            })
            .then(result => {
                axios
                    .post(url, {
                        email: result.email,
                        password: result.password,
                        feedId: feedId
                    })
                    .then(() => dispatch(likeSuccess()))
                    .catch(err => console.log(err.response.data))
            })
    }
}

export const commentFeed = (feedId, comment) => {
    const url = `${server}/comment`
    return dispatch => {
        dispatch(commentProcess())
        AsyncStorage
            .getItem('USER')
            .then(result => {
                return JSON.parse(result)
            })
            .then(result => {
                axios
                    .post(url, {
                        email: result.email,
                        password: result.password,
                        feedId: feedId,
                        comment: comment
                    })
                    .then(() => dispatch(commentSuccess()))
                    .catch(err => console.log(err.response.data))
            })
    }
}