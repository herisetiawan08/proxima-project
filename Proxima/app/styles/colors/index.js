const mainColor = '#c62828'
const secColor = '#fff'
const thirdColor = '#b71c1c'
const forthColor = '#aaa'

export { mainColor, secColor, thirdColor, forthColor }