import React, { Component } from 'react'
import { KeyboardAvoidingView, ActivityIndicator, Alert, TextInput, Text, View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import style from '../styles'
import { email, password } from '../validators'
import { mainColor, secColor, thirdColor } from '../styles/colors'
import { userRegister } from '../action/auth'

class Register extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            repassword: '',
            buttonPressed: false,
        }
    }

    validEmail = () => {
        return this.state.emailChange ? email.test(this.state.email) : !this.state.emailChange
    }

    validPass = () => {
        return this.state.passwordChange ? password.test(this.state.password) : !this.state.passwordChange
    }

    validRepass = () => {
        return this.state.password === this.state.repassword
    }

    buttonDisable = () => {
        if (
            email.test(this.state.email) &&
            password.test(this.state.password) &&
            this.state.password === this.state.repassword
        ) {
            return false
        } else {
            return true
        }
    }

    Register = () => {
        this.props.userRegister(this.state.email, this.state.password)
        this.setState({
            buttonPressed: true
        })
    }

    render() {
        if (this.props.user.loading) {
            return (
                <View style={[style.fullContainer, style.container]}>
                    <ActivityIndicator color={secColor} size='large' />
                </View>
            )

        } else {
            if (this.state.buttonPressed) {
                if (this.props.user.auth) {
                    this.props.navigation.navigate('ProfileEditor')
                } else {
                    Alert.alert('Failed', this.props.user.payload)
                    this.setState({
                        buttonPressed: false
                    })
                }
            }
        }
        return (
            <View style={[style.fullContainer, style.container]}>
                <Text style={style.titleHeader}>
                    Register
                </Text>
                <View style={style.formContainer}>
                    <TextInput
                        style={style.basicInput}
                        placeholder='Email'
                        underlineColorAndroid={secColor}
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })}
                        keyboardType='email-address'
                        onBlur={() => {
                            this.setState({ emailChange: true })
                        }}
                        onSubmitEditing={() => {
                            this.passwordType.focus();
                        }}
                        returnKeyType='next'
                    />
                    {this.validEmail() ? null :
                        <Text style={style.bottomWarning}>
                            *Only using Real University email
                        </Text>}
                    <TextInput
                        style={style.basicInput}
                        placeholder='Password'
                        underlineColorAndroid={secColor}
                        value={this.state.password}
                        onChangeText={password => this.setState({ password })}
                        ref={(input) => { this.passwordType = input; }}
                        onBlur={() => {
                            this.setState({ passwordChange: true })
                        }}
                        onSubmitEditing={() => {
                            this.repasswordType.focus();
                        }}
                        returnKeyType='next'
                        secureTextEntry={true}
                    />
                    {this.validPass() ? null :
                        <Text style={style.bottomWarning}>
                            *Password must be have at least 6 character
                        </Text>}
                    <TextInput
                        style={style.basicInput}
                        placeholder='Re-enter Password'
                        underlineColorAndroid={secColor}
                        value={this.state.repassword}
                        ref={(input) => { this.repasswordType = input; }}
                        onChangeText={repassword => this.setState({ repassword })}
                        onBlur={() => {
                            this.setState({ repasswordChange: true })
                        }}
                        secureTextEntry={true}
                    />
                    {this.validRepass() ? null :
                        <Text style={style.bottomWarning}>
                            *Password doesn't match
                        </Text>}
                    <View style={style.buttonMargin} />
                    <View style={this.buttonDisable() ? [style.pressButton, style.disabled] : [style.pressButton]}>
                        <TouchableOpacity onPress={this.Register} disabled={this.buttonDisable()}>
                            <Text style={style.pressButtonTxt}>
                                Register
                            </Text>

                        </TouchableOpacity>
                    </View>

                </View>
            </View>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

mapDispatchToProps = (dispatch) => {
    return {
        userRegister: (email, password) => {
            dispatch(userRegister(email, password))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Register)