import React, { Component } from 'react'
import { Image, Text, View, TouchableOpacity, ActivityIndicator, ToastAndroid } from 'react-native'
import style from '../styles'
import { connect } from 'react-redux'
import { getFeedFollow, addFollower } from '../action/user'
import { mainColor, secColor, thirdColor, forthColor } from '../styles/colors'
import AsyncStorage from '@react-native-community/async-storage'
import { server } from '../config'
import RNRestart from 'react-native-restart'
import { NavigationEvents } from 'react-navigation'

class UserProfile extends Component {
    static navigationOptions = {
        header: null
    }

    state = {
        email: '',
        password: '',
        self: {},
        friend: false
    }

    whenScreenFocus() {
        AsyncStorage
            .getItem('USER')
            .then(result => {
                return JSON.parse(result)
            })
            .then(result => {
                {
                    this.setState({ email: result.email, password: result.password })
                }
            })
        if (this.props.navigation.state.params !== undefined) {
            this.props.getFeedFollow(this.props.navigation.state.params.email)
            this.setState({ self: this.props.navigation.state.params, friend: true })
        } else {
            AsyncStorage
                .getItem('USER')
                .then(result => {
                    return JSON.parse(result)
                })
                .then(result => {
                    {
                        this.props.getFeedFollow(result.email)
                        this.setState({ self: result })
                    }
                })
        }
    }

    onPressButton = () => {
        if (this.state.friend) {
            this.props.addFollower(this.state.email, this.state.password, this.state.self.email)
            ToastAndroid.show(`You have followed ${this.state.self.email}`)
            RNRestart.Restart()

        } else {
            AsyncStorage.removeItem('USER')
            AsyncStorage.removeItem('LastPlace')
            RNRestart.Restart()
        }

    }

    imagePicker = () => {
        const camOptions = {
            title: 'Change Picture',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(camOptions, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                const source = { uri: response.uri };
                this.props.uploadDisplayPicture(source, this.state.self.email, this.state.self.password)
                this.setState({
                    avatarSource: source,
                });
            }
        });
    }

    render() {
        if (this.props.find.loading || this.props.find.getFolFeed == undefined) {
            return (
                <View style={[style.fullContainer, style.container]}>
                    <NavigationEvents
                        onWillFocus={() => this.whenScreenFocus()}
                    />
                    <ActivityIndicator color={secColor} size='large' />
                </View>
            )
        }
        return (
            <View style={[style.fullContainer, style.container]}>
                <NavigationEvents
                    onWillFocus={() => this.whenScreenFocus()}
                />
                <View style={style.secContainer}>
                    <Image source={
                        this.state.self.profilePic ?
                            { uri: `${server}/image/profileImg/${this.state.self.profilePic}` } :
                            require('../src/img/default-profile-picture.jpg')
                    } style={[style.profilePic]}
                    />
                    <Text style={[style.titleHeader, style.titleHeaderAlt]}>
                        {this.state.self.firstName} {this.state.self.lastName}
                    </Text>
                    <Text style={style.emailProfile}>
                        {this.state.self.email}
                    </Text>
                    <View style={style.miniBoxContainer}>
                        <View style={style.miniBoxContainerSub}>
                            <Text style={style.miniBoxNumber}>
                                {(this.props.find.getFolFeed.feedCount).length}
                            </Text>
                            <Text style={style.miniBoxTitle}>
                                FEED
                        </Text>
                        </View>
                        <View style={style.miniBoxContainerSub}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('ListUser', this.props.find.getFolFeed.following)}>
                                <Text style={style.miniBoxNumber}>
                                    {(this.props.find.getFolFeed.following).length}
                                </Text>
                            </TouchableOpacity>
                            <Text style={style.miniBoxTitle}>
                                FOLLOWING
                        </Text>
                        </View>
                        <View style={style.miniBoxContainerSub}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('ListUser', this.props.find.getFolFeed.followers)}>
                                <Text style={style.miniBoxNumber}>
                                    {(this.props.find.getFolFeed.followers).length}
                                </Text>
                            </TouchableOpacity>
                            <Text style={style.miniBoxTitle}>
                                FOLLOWER
                        </Text>
                        </View>
                    </View>
                    {!this.state.friend &&
                        <View style={[style.pressButton, style.pressButtonAlt, style.elevate, { marginTop: 20 }]}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('ProfileEdit', this.state.self)}>
                                <Text style={[style.pressButtonTxt, style.pressButtonTxtAlt]}>
                                    Edit Profile
                            </Text>
                            </TouchableOpacity>
                        </View>
                    }
                    <View style={[style.pressButton, style.pressButtonAlt, style.elevate]}>
                        <TouchableOpacity onPress={this.onPressButton} disabled={this.state.friend ? ((this.props.find.getFolFeed.followers).includes(this.state.email) ? true : false) : false}>
                            <Text style={[style.pressButtonTxt, style.pressButtonTxtAlt]}>
                                {this.state.friend ? ((this.props.find.getFolFeed.followers).includes(this.state.email) ? 'Followed' : 'Follow') : 'Log Out'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        find: state.find
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getFeedFollow: (email) => {
            dispatch(getFeedFollow(email))
        },
        addFollower: (email, password, target) => {
            dispatch(addFollower(email, password, target))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile)