import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, TextInput, ToastAndroid } from 'react-native'
import style from '../styles'
import { connect } from 'react-redux'
import { imageRemove, newFeed } from '../action/feed'

class NewFeed extends Component {
    static navigationOptions = {
        header: null
    }
    state = {
        caption: ''
    }

    componentDidMount() {
    }

    componentWillUnmount() {
        this.props.imageRemove()
        !this.state.buttonPressed ? ToastAndroid.show('Creating Feed is Interupted', ToastAndroid.LONG) : null

    }

    onPressButton = async () => {
        this.setState({
            buttonPressed: true
        })
        this.props.newFeed(this.state.caption, this.props.feed.image)
        this.props.navigation.navigate('Feed')
    }

    render() {
        return (
            <View style={[style.fullContainer, style.container]}>
                <View style={style.secContainer}>
                    <Image source={this.props.feed.image} style={style.feedImage} />
                    <TextInput
                        value={this.state.caption}
                        placeholder='Describe your photo here'
                        multiline={true}
                        style={[style.basicInput, style.captionInput]}
                        onChangeText={(caption) => this.setState({ caption: caption })}
                    />
                    <View style={[style.pressButton, style.pressButtonAlt, style.elevate, style.moveToBottom]}>
                        <TouchableOpacity onPress={this.onPressButton}>
                            <Text style={[style.pressButtonTxt, style.pressButtonTxtAlt]}>
                                Post New Feed
                        </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        feed: state.feed
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        imageRemove: () => {
            dispatch(imageRemove())
        },
        newFeed: (caption, image) => {
            dispatch(newFeed(caption, image))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewFeed)