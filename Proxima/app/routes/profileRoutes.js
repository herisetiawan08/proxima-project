import React, { Component } from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Profile from '../components/UserProfile'
import ProfileEdit from '../components/UserEditProfile'
import ListUser from '../components/ListUser'

const profileNavigator = createStackNavigator(
    {
        Profile: { screen: Profile },
        ProfileEdit: { screen: ProfileEdit },
        ListUser: {screen: ListUser}
    },
    {
        initialRouteName: 'Profile'
    }
)

const profileRoutes = createAppContainer(profileNavigator)

export default profileRoutes