const { User } = require('../models/user')
const md5 = require('md5')
const btoa = require('btoa')
const { checkEmail, checkUsername } = require('../validators/checker')
const { email, password } = require('../validators/index')
const path = require('path')
const fs = require('fs')

exports.register = async (request, h) => {
    if ((await checkEmail(request.payload.email))[0] === undefined) {
        try {
            const id = Math.floor(Math.random() * (10 ** 16))
            const password = await md5(request.payload.password)
            Object.assign(request.payload, { id: id, password: password })
            const newUser = new User(request.payload)
            await newUser.save()
            h.response().code(200)
            return newUser
        } catch (err) {
            return h
                .response(
                    [
                        {
                            auth: false,
                            message: err
                        }
                    ]
                )
        }
    } else {
        return h.response([{ auth: false, message: 'Email already used' }]).code(409)
    }
}

exports.login = async (request, h) => {
    const { email, password } = request.payload
    const emailFind = await checkEmail(email)
    if (emailFind[0] !== undefined) {
        if (md5(password) === emailFind[0].password) {
            return h.response([{ auth: true, user: emailFind[0] }]).code(200)
        } else {
            return h.response([{ auth: false, message: 'Invalid email or password' }]).code(401)
        }
    } else {
        return h.response([{ auth: false, message: 'Email not registered' }]).code(400)
    }
}
