const mongoose = require('mongoose')
const { Schema } = mongoose

const feedModel = new Schema({
    feedId: Number,
    owner: String,
    imgUrl: String,
    caption: String,
    createdAt: Date,
    updatedAt: Date,
    likes: Array,
    comments: Array
})

const Feed = mongoose.model('feed', feedModel)

module.exports = { Feed }