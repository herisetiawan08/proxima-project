const { register, login } = require('../controllers/auth')

module.exports = [
    {
        method: 'POST',
        path: '/login',
        handler: login
    },
    {
        method: 'POST',
        path: '/register',
        handler: register
    }
]