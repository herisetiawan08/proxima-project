const { User } = require('../models/user')

exports.checkEmail = async (value) => {
    return await User
        .find({ email: value })
        .catch(err => {
            return h.response([{ auth: false, message: err }])
        })
}

exports.checkUsername = async (value) => {
    return await User
        .find({ username: value })
        .catch(err => {
            return h.response([{ auth: false, message: err }])
        })
}