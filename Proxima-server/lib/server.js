const Hapi = require('@hapi/hapi')
const routes = require('../routes')
const { mongodb } = require('./mongo')
const Path = require('path')


const server = Hapi.server({
    port: 3000,
    host: '0.0.0.0',
    routes: {
        files: {
            relativeTo: Path.join(__dirname, '../src/')
        }
    },
})

const register = async () => {
    await server.register([
        require('inert'),
    ]);
}


const main = async () => {
    try {
        mongodb()
        await register()
        server.route(routes)
        await server.start()
        console.log(`Server running at: ${server.info.uri}`)
        return server
    } catch (err) {
        console.error(err)
        process.exit(1)
    }
}

module.exports = { main }